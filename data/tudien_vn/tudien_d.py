"""
load .../tudien_vn/gitcloned/tudien/tudien.txt
write into tudien_d.json
"""
import json


def txt_to_dict(file_path):
  """Converts a txt file into a Python dictionary.

  Args:
    file_path: The path to the txt file.

  Returns:
    A Python dictionary.
  """

  with open(file_path, "r",encoding = 'utf-8') as f:
    lines = f.readlines()

  dict = {}
  for line in lines:
    key = line
    key = key.strip()
    value = ''
    dict[key] = value

  return dict


# Example usage:

dict = txt_to_dict(r'C:\Users\Admin\Documents\py_scripts\btx_chatbot\data\tudien_vn\gitcloned\tudien\tudien.txt')

# Print the dictionary:

with open('tudien_d.json' , 'w', encoding = 'utf-8') as out:
  json.dump(dict, out, ensure_ascii=False, indent = 2)
