from pathlib import Path


def extract_keyword(f:Path):
  s = f.read_text()

  '''
  #TODO code here @ get vietnamese words from var :s eg we could get 'địa chỉ' word
  thử cắt chuỗi :s ra các từ vietnam, tìm cách cắt "hợp lý" / "có ý nghĩa nhất"

  # words = s.split(' ')  # we dont have 'địa chỉ' by a simple .split(' ') ie  > ['ref', 'https://thptbuithixuan.hcm.edu.vn/lienhe\n\n>', 'Địa', 'chỉ', 'nhà', 'trường:', 'Số', '73', 'đường', 'Bùi', 'Thị', 'Xuân,', 'phường', 'Phạm', 'Ngũ', 'Lão,', 'quận', '1,', 'thành', 'phố', 'Hồ', 'Chí', 'Minh.\n']
  '''
  def cat_tu(splitted):
    """
    chữ cái   a, b, c, ...
    từ        ba, mẹ   aka từ đơn
    từ ghép   câu lạc bộ, địa chỉ, nhà trường
              ie từ có nhiều từ đơn

    --- idea
    nguyenlieutu = splitted = ['Địa', 'chỉ', 'nhà', 'trường:', 'Số', '73', 'đường', ... ]

    batdau voi tu co dodai l=1,2,3, v.v..

    dodai 1   địa           địa -  chỉ - nhà ...

    dodai 2   địa chỉ       địa chỉ - nhà - trường ...
              địa chỉ       địa chỉ - nhà trường -

    dodai 3   địa chỉ nhà   địa chỉ nhà - trường ...
              VO LY
              tu nay KO CO TRONG TUDIEN
    """
    splitted = s.split(' ')
    todo=122

  def chon_keyword(s):
    todo=122

  vn_word_list = cat_tu(s)
  keyword_list = chon_keyword(vn_word_list)
  return keyword_list

if __name__=='__main__':
  SH=Path(__file__).parent

  for f in (SH/'knowledge-source').glob('*'):
    keyword_list = extract_keyword(f)
